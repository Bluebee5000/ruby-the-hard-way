# "puts" tell the machine to 'put' as in print/output a string which is the 's' in puts.
puts "Hello World!"
puts "Hello Again"
puts "I like typing this."
puts "This is fun."
puts "Yay! Printing."
puts "I'd must rather you 'not'."
puts 'I "said" do not touch this.'

# Study drills #
puts "World 1 map is almost finished."
# Make it only print one line by commenting out all but one of teh lines wow
# an octothorpe character (#) makes a comment in ruby files.