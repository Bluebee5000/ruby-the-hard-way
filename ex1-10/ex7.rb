# returns a string
puts "Mary had a little lamb."
# returns a string using unnecassary characters around the word snow
puts "Its fleece was white as #{'snow'}."
# returns a string
puts "And everywhere that Mary went."
# returns the string 10 times
puts "." * 10 # what'd that do?

# end1 through end12 defines a sequence of strings that equals "CheeseBurger"
end1 = "C"
end2 = "h"
end3 = "e"
end4 = "e"
end5 = "s"
end6 = "e"
end7 = "B"
end8 = "u"
end9 = "r"
end10 = "g"
end11 = "e"
end12 = "r"

# watch that print vs. puts on this line what's it do?
# outputs a sequence of strings that equals "CheeseBurger"
# 'print' returns the sequence of end1 through end6 on the same line as the return 
# from end7 through end12 without a space
print end1 + end2 + end3 + end4 + end5 + end6
puts end7 + end8 + end9 + end10 + end11 + end12

# Study Drills #
# wrote comments  explaining what each line does
# read the program backwards looking for errors. None to be found.
# remember to learn from your mistakes