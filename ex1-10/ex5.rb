name = 'Zed A. Shaw'
age = 35 # not a lie in 2009
height = 74 # inches
weight = 180 # lbs
eyes = 'Blue'
teeth = 'White'
hair = 'Brown'
height_centimeters = 74 * 2.54
weight_kilograms = 1 / 2.2046226218 * 180

puts "Let's talk about #{name}."
puts "He's #{height} inches, which is #{height_centimeters} centimeters, tall."
puts "He's #{weight} pounds, which is #{weight_kilograms} kilograms, heavy."
puts "Actually that's not too heavy."
puts "He's got #{eyes} eyes and #{hair} hair."
puts "His teeth are usually #{teeth} depending on the coffee."

# this line is tricky, try to get it exactly right
puts "if I add #{age}, #{height}, and #{weight} I get #{age + height + weight}."

# Study Drills #
# I changed all the variables to not have the 'my_' in front.
# wrote new variables to say his height in centimeters and his weight in kilograms.