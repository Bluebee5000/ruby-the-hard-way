# defines var formatter to equal a string
formatter = "%{first} %{second} %{third} %{fourth}"

# returns "1 2 3 4"
puts formatter % {first: 1, second: 2, third: 3, fourth: 4}
# returns "one two three four"
puts formatter % {first: "one", second: "two", third: "three", fourth: "four"}
# returns "true false true false"
puts formatter % {first: true, second: false, third: true, fourth: false}
# returns the value of var formatter 4 times on the same line
puts formatter % {first: formatter, second: formatter, third: formatter, fourth: formatter}

# returns the string of text one after another on the same line
# and is coded neatly
puts formatter % {
  first: "I had this thing.",
  second: "That you could type up right.",
  third: "But it didn't sing.",
  fourth: "So I said goodnight."
}

# Study Drills #
# Commented each line explaining what it does
# Checked for mistakes and kept in mind that I must learn form them when I make them