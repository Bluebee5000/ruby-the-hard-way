cars = 100
space_in_a_car = 4.0
drivers = 30
passengers = 90
cars_not_driven = cars - drivers
cars_driven = drivers
carpool_capacity = cars_driven * space_in_a_car
average_passengers_per_car = passengers / cars_driven


puts "There are #{cars} cars available."
puts "There are only #{drivers} drivers available."
puts "There will ne #{cars_not_driven} empty cars today."
puts "We can transport #{carpool_capacity} people today."
puts "We have #{passengers} to carpool today."
puts "We need to put about #{average_passengers_per_car} in each car."

# Study Drills #
# That undefined local variable error means you spelled the variable wrong in the puts statemant.
# sing the .0 will make ruby return a precise number like 3.625 for the purposes of this program I don't see any reason wh it was necassary.
# Used irb to do math:
# a = 50 
# (puts) a * 2.0 + 10 * 10
# returned: 200.0