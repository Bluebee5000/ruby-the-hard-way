# returns the string of text in the quote signs
puts "I will now count my chickens:"

# returns the text 'Hens' followed by the result of the equation due to the octothorpe and brackets
puts "Hens #{25.0 + 30 / 6}"
# returns the text 'Roosters' followed by the result of the equation due to the octothorpe and brackets
puts "Roosters #{100.0 - 25 * 3 % 4}"

# returns the string of text in the quote signs
puts "Now I will count the eggs:"

# returns the result of the equation
puts 3.0 + 2 + 1 - 5 + 4 % 2 - 1 / 4 + 6

# returns the string of text and numbers in the quote signs
puts "Is it true that 3 + 2 < 5 - 7?"

# returns the result of the equation
puts 3.0 + 2 < 5 - 7

# returns the text 'What is 3 + 2?' followed by the result of the equation due to the octothorpe and brackets
puts "What is 3 + 2? #{3.0 + 2}"
# returns the text 'What is 5 - 7' followed by the result of the equation due to the octothorpe and brackets
puts "What is 5 - 7? #{5.0 - 7}"

# returns the string of text in the quote marks
puts "Oh, that's why it's false."

# returns the string of text inside the quote marks
puts "How about some more."


# returns the text 'Is it greater?' followed by the result of the equation due to the octothorpe and brackets
puts "Is it greater? #{5.0 > -2}"
# returns the text 'Is it greater or equal?' followed by the result of the equation due to the octothorpe and brackets
puts "Is it greater or equal? #{5.0 >= -2}"
# returns the text 'Is it less or equal?' followed by the result of the equation due to the octothorpe and brackets
puts "Is is less or equal? #{5.0 <= -2}"

# Study Drills #
# 1: I used comments to explain what each line does to myself.
# 2: Started irb and used ruby as a calculator.
# 3: rewrote the program to use floating point numbers to give precise answers.