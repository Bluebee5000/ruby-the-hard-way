# Here's some new strange stuff, remember type it exactly.

# var days defined to equal the week abreviations
days = "Mon Tue Wed Thu Fri Sat Sun"
# var months defined to equal a string of the month abreviations with \n in between each for a line break
months = "Jan\nFeb\nMar\nApr\nMay\nJun\nJul\nAug"

# a string that outputs the bvalue of var days
puts "Here are the days: #{days}"
# a string that ouputs the value of var months
puts "Here are the months: #{months}"

# Uses 3 double-quotes for a block of strings
puts """
There's something going on here.
With the three double-quotes.
We'll be able to type as much as we like.
Even 4 lines if we want, or 5, or 6.
"""

# Study Drills #
# Commented every line
# 1 mistake: an extra space on the first variable line. Will not make that mistake again