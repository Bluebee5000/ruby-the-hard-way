tabby_cat = "\tI'm tabbed in."
persian_cat = "I'm split\non a line."
backslash_cat = "I'm \\ a \\ cat."

fat_cat = """
I'll do a list:
\t* Cat food
\t* Fishies
\t* Catnip\n\t* Grass
"""

puts tabby_cat
puts persian_cat
puts backslash_cat
puts fat_cat

# Study Drills #
# Memorize all escape sequences with flashcards
# Use three single-quotes instead of three double-quotes. Can you see why you might use that?
# Combine escape sequences and format strings to create a more complex format.
line_end = "End of exercise."
line_beginning = "Beginning ot my own code."

puts line_end
puts line_beginning

x = "He said, 'They are smart.' But I did not aggree"
h = "\nh\ne\n \ni\ns\n \ns\nm\na\nr\nt"

puts x
puts h