# var 'types_of_people' defined to equal '10'
types_of_people = 10
# var 'x' defined to equal a string that includes the value of the 'types_of_people' var
x = "There are #{types_of_people} type of people."
# var 'binary' defined to equal a string
binary = "binary"
# var 'do_not' defined to equal a string
do_not = "don't"
# var 'y' defined to equal a string that includes the values of the 'binary' and the 'do_not' var
y = "Those the know #{binary} and those who #{do_not}."

# outputs value of the var 'x'
puts x
# outputs value of the var 'y'
puts y

# outputs a string that includes the value of th var 'x'
puts "I said: #{x}."
# outputs a string that includes the value of the var 'y'
puts "I also said: '#{y}'."

# var 'hilarious' defined to equal 'false'
hilarious = false
# var 'joke_evaluation' defined to equal a string that includes the value of the var 'hilarious'
joke_evaluation = "Isn't that joke so funny?! #{hilarious}"

# outputs the value of the var 'joke_evaluation'
puts joke_evaluation

# var 'w' defined to equal a string of text
w = 'This is the left side of...'
# var e defined the equal a string of text
e = 'a string with a right side.'

# adds the values of var 'w' and 'e'
puts w + e

# Study Drills #
# I wrote comments explaining teh fuction of each line of code
# Instances of string inception: lines 18, 20, 25 and 10(had 2 strings in it)
# Yes there are only 4 places where a string is put in a string. The variables 'hilarious' and 'types_of_people' do not equal strings.
# adding 'w + e' returns a longer string because both variable equal strings of text which cannot be added in the way numbers can be added to on another so ruby just puts the value of var 'e' immediatly after the value of var 'w' creating a longer string. 1 + 1 = 2 but a string of text + a string of text will only output a longer string of text.
# Nothing happens when you change the quote marks on a string from double to single because ruby understands both double and single quote marks to mean the same thing when it comes to strings.