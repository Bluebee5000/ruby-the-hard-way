# A comment, this is so you can read you program later.
# Anything after the # is ignored by ruby.

puts "I could have code like this." # and the comment after is ignored

# You can also use a comment to "disable" or comment out a piece of code:
# puts "This won't run."

puts "This will run."

# Study Drills #
# 1: I know a # makes comments in the code and is called a pound sign, hash or octothorpe.
# 2: I reviewed my code backwards.
# 3: Found no mistakes.
# 4: I read my code out loud including the #'s and found no mistakes.