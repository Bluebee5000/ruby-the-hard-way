### Ruby code Notes ###
#1: "puts" means print (put) a string (s)
  code: puts "this is the string that will be printed when the file is run"

#2: Names of common math symbols in code:
  * + plus
  * - minus
  * / slash (for division)
  * * asterisk (for multiplication)
  * % percent
  * < less-than
  * > greater-than
  * <= less-than-equal
  * >= greater-than-equal

#3: This: puts "What is 5 + 3? #{5 + 3}" will output this: What is 5 + 3? 8
  This syntax: #{} is how you tell ruby that you want to get the result of the the input returned in the string.

#4: When doing math calculations with ruby
  For the putput to be precise you need to use a decimal point in the first number like this:
  7.0 / 2 will output 3.5
  but
  7 / 2 will output 3 (which is wrong)
  Without the . ruby will only deal in whole numbers
  
#5: Escape characters:
* \\	Backslash ()
* \'	Single-quote (')
* \"	Double-quote (")
* \a	ASCII bell (BEL)
* \b	ASCII backspace (BS)
* \f	ASCII formfeed (FF)
* \n	ASCII linefeed (LF)
* \r	ASCII Carriage Return (CR)
* \t	ASCII Horizontal Tab (TAB)
* \uxxxx	Character with 16-bit hex value xxxx (Unicode only)
* \v	ASCII vertical tab (VT)
* \ooo	Character with octal value ooo
* \xhh	Character with hex value hh