# README # ruby-the-hard-way #

* ruby-the-hard-way is a collection of folders and files for me to put the ruby exercises I do from http://learnrubythehardway.org/book/

* See http://learnrubythehardway.org/book/intro.html for details on how to go through the exercises for max benefit.