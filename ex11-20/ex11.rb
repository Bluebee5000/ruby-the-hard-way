# print "What is your age in years? "
# age = gets.chomp
# print "How tall are you in feet and inches? "
# height = gets.chomp
# print "How much do you weigh? "
# weight = gets.chomp
# print "Are those pounds or kilograms? "
# weight_unit = gets.chomp
# puts "So, you're #{age} years old, #{height} tall and #{weight} #{weight_unit} heavy."

# Study Drills #
# gets.chomp returns a string that was input form the user of the program
# modified the program with my own gets.chomp operations
# wrote a new "form" programs:
wait = gets.chomp wait#s for enter

print "Is it soft? yes/no: "
q1 = gets.chomp
if q1 == "yes"
  puts "OK"
else
  puts "OK it is not soft."
end

wait = gets.chomp wait#s for enter

print "Is it very large? yes/no: "
q2 = gets.chomp
if q2 == "yes"
  puts "OK"
else
  puts "OK"
end

wait = gets.chomp wait#s for enter

print "Is it tall? yes/no: "
q3 = gets.chomp
if q3 == "yes"
  puts "OK"
else
  puts "OK it is not tall."
end

wait = gets.chomp wait#s for enter

print "I know what the answer is. May I guess the answer? yes/no: "
guess = gets.chomp
if guess == "yes"
  puts "It is a rabbit. yes?"
else
  puts "'no' what do you mean 'no' I will answer anyways."
  wait = gets.chomp wait#s for enter
  puts "It is a rabbit. yes/no: "
end
answer = gets.chomp
if answer == "yes"
  puts "I knew it!"
else
  puts "Darn. I will get it next time."
end